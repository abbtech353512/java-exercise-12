package org.example;

import org.example.BusinessLayer.ApplicationServiceImpl;
import org.example.testLearning.CalculatorService;

public class Main {
    public static void main(String[] args) {
        CalculatorService c = new CalculatorService();
        System.out.println(c.multiply(3, 4));

        ApplicationServiceImpl a = new ApplicationServiceImpl(c);
        System.out.println(a.multiply(3, 4));
    }
}