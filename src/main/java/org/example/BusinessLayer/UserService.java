package org.example.BusinessLayer;

public class UserService {
    private UserRepository _repo;

    public UserService(UserRepository repo) {
        _repo = repo;
    }

    public boolean isUserActive(String username) {
        User user = _repo.findByUsername(username);
        return user != null && user.isActive();
    }

    public void deleteUser(long userId) throws Exception {
        User user = _repo.findByUserId(userId);

        if (user == null) {
            throw new Exception();
        }
    }

    public User getUser(long userId) throws Exception {
        User user = _repo.findByUserId(userId);

        if (user == null) {
            throw new Exception();
        }

        return user;
    }
}
