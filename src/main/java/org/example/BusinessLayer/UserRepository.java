package org.example.BusinessLayer;

public class UserRepository {
    public User findByUsername(String username) {
        if (username.length() < 10)
            return new User();
        else return null;
    }

    public User findByUserId(long userId) {
        if (userId < 10)
            return new User();
        else return null;
    }
}
