package org.example.BusinessLayer;

import org.example.testLearning.CalculatorService;

public class ApplicationServiceImpl {
    private CalculatorService calculator;

    public ApplicationServiceImpl(CalculatorService service) {
        this.calculator = service;
    }

    public int multiply(int a, int b) {
        if (a * b == 0) {
            // Calling calc-multiply function for only mocking-verify
            calculator.multiply(a, b);
            calculator.multiply(a, b);
            calculator.multiply(a, b);
            calculator.multiply(a, b);
            throw new ArithmeticException("cannot -> a = 0 or b = 0");
        }

        return calculator.multiply(a, b);
    }
}
