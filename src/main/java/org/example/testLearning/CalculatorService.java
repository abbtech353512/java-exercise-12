package org.example.testLearning;

public class CalculatorService {
    public int add(int a, int b) {
        long result = (long)a + b;
        CheckRange(result);
        return (int) result;
    }

    public int subtract(int a, int b) {
        long result =  (long)a - b;
        CheckRange(result);
        return (int) result;
    }

    public int multiply(int a, int b) {
        long result = (long) a * b;
        CheckRange(result);
        return (int) result;
    }

    public float divide(int a, int b) {
        if (b == 0) {
            throw new IllegalArgumentException("Denominator cannot be zero.");
        }
        double result = (double) a / b;
        CheckRange(result);
        return (float) result;
    }

    private <T extends Number> void CheckRange(T parameter) {
        if (parameter instanceof Long) {
            long converted = (long) parameter;
            if (converted > Integer.MAX_VALUE || converted < Integer.MIN_VALUE) {
                throw new RuntimeException("Overflow occurs with parameter: " + converted);
            }
        }
        else if (parameter instanceof Double) {
            double converted = (double) parameter;
            if (converted > Float.MAX_VALUE || converted < Float.MIN_VALUE) {
                throw new RuntimeException("Overflow occurs with parameter: " + converted);
            }
        }
    }
}
