package org.example.BusinessLayer;

import org.example.testLearning.CalculatorService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ApplicationServiceUnitTest {

    @Mock
    private CalculatorService calculator;
    @InjectMocks
    private ApplicationServiceImpl appService;

    @Test
    public void multiply_success() {
        when(calculator.multiply(anyInt(), anyInt())).thenReturn(12);
        int actual = appService.multiply(3, 5);
        assertEquals(12, actual);
        verify(calculator, times(1)).multiply(3, 5);
    }
    @ParameterizedTest
    @CsvSource(value = {"1-0-0", "0-10-0", "0-10-0"}, delimiter = '-')
    void multiply_exception(int a, int b, int result) {
        when(calculator.multiply(a, b)).thenReturn(result);
        assertThrows(ArithmeticException.class, () -> {
            appService.multiply(a, b);
        });

        verify(calculator, times(4)).multiply(a, b);
    }
}
