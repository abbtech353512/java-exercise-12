package org.example.BusinessLayer;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceUnitTest {
    @Mock private UserRepository repository;
    @InjectMocks private UserService service;

    @Test
    void isUserActive_success() {
        String cachedAnyString = anyString();
        when(repository.findByUsername(cachedAnyString)).thenReturn(new User());
        var actual = service.isUserActive(cachedAnyString);
        assertTrue(actual);

        verify(repository, times(1)).findByUsername(cachedAnyString);
    }

    @Test
    void isUserActive_fail() {
        String cachedAnyString = anyString();
        when(repository.findByUsername(cachedAnyString)).thenReturn(null);
        var actual = service.isUserActive(cachedAnyString);
        assertFalse(actual);

        verify(repository, times(1)).findByUsername(cachedAnyString);
    }

    @ParameterizedTest
    @CsvSource(value = {"'1234'"})
    void isUserActive_parameterized_success(String username) {
        when(repository.findByUsername(username)).thenCallRealMethod();
        var actual = service.isUserActive(username);
        assertTrue(actual);

        verify(repository, times(1)).findByUsername(username);
    }

    @ParameterizedTest
    @CsvSource(value = {"12345678901234567890"})
    void isUserActive_parameterized_fail(String username) {
        when(repository.findByUsername(username)).thenCallRealMethod();
        var actual = service.isUserActive(username);
        assertFalse(actual);

        verify(repository, times(1)).findByUsername(username);
    }

    @ParameterizedTest
    @CsvSource(value = {"5"})
    void deleteUser_success(long userId) {
        when(repository.findByUserId(userId)).thenCallRealMethod();
        assertDoesNotThrow(() -> {
            service.deleteUser(userId);
        });

        verify(repository, times(1)).findByUserId(userId);
    }

    @ParameterizedTest
    @CsvSource(value = {"25"})
    void deleteUser_fail(long userId) {
        when(repository.findByUserId(userId)).thenCallRealMethod();
        assertThrows(Exception.class, () -> {
            service.deleteUser(userId);
        });

        verify(repository, times(1)).findByUserId(userId);
    }

    @Test
    void getUser_success() throws Exception {
        when(repository.findByUserId(anyLong())).thenReturn(new User());

        var user = service.getUser(anyLong());
        assertInstanceOf(User.class, user);

        verify(repository, times(1)).findByUserId(anyLong());
    }

    @Test
    void getUser_fail() throws Exception {
        when(repository.findByUserId(anyLong())).thenReturn(null);

        assertThrows(Exception.class, () -> {
            var user = service.getUser(anyLong());
        });

        verify(repository, times(1)).findByUserId(anyLong());
    }
}
