package org.example.testLearning;

import static java.lang.System.out;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class CalculatorServiceTest {

    private CalculatorService service;

    @BeforeEach
    void init() {
        service = new CalculatorService();
    }

    @Test
    void multiplyTest() {
        int actualResult = service.multiply(10, 5);
        assertEquals(50, actualResult);

        assertThrows(RuntimeException.class, () -> {
           int boundaryCase = service.multiply(Integer.MAX_VALUE, 2);
        });
    }

    @Test
    void addTest() {
        int actualResult = service.add(5, 10);
        assertEquals(15, actualResult);

        assertThrows(RuntimeException.class, () -> {
            int boundaryCase = service.add(Integer.MAX_VALUE, 1);
        });
    }

    @Test
    void subtractTest() {
        int actual = service.subtract(10, 5);
        assertEquals(5, actual);

        assertThrows(RuntimeException.class, () -> {
           int boundaryCase = service.subtract(Integer.MIN_VALUE, 1);
        });
    }

    @Test
    void divideTest() {
        float actual = service.divide(10, 5);
        assertEquals(2.0f, actual);

        assertThrows(IllegalArgumentException.class, () -> {
            float zeroDenominatorCase = service.divide(10, 0);
        });
    }
}