plugins {
    id("java")
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("org.mockito:mockito-core:3.5.13")
    testImplementation("org.mockito:mockito-junit-jupiter:2.17.0")
}

tasks.test {
    useJUnitPlatform() {
        excludeTags("disabled")
    }
}